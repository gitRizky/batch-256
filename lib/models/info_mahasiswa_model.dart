class InfoMahasiswaModel {
  int id;
  String nama;
  String tanggal_lahir;
  String gender;
  String alamat;
  String jurusan;
  String foto_path;

  InfoMahasiswaModel(
      {this.id,
      this.nama,
      this.tanggal_lahir,
      this.gender,
      this.alamat,
      this.jurusan,
      this.foto_path});

  //setter
  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['nama'] = nama;
    data['tanggal_lahir'] = tanggal_lahir;
    data['gender'] = gender;
    data['alamat'] = alamat;
    data['jurusan'] = jurusan;
    data['foto_path'] = foto_path;

    return data;
  }
  //getter

  InfoMahasiswaModel.fromMap(Map<String, dynamic> data) {
    this.id = data['id'];
    this.nama = data['nama'];
    this.tanggal_lahir = data['tanggal_lahir'];
    this.gender = data['gender'];
    this.alamat = data['alamat'];
    this.jurusan = data['jurusan'];
    this.foto_path = data['foto_path'];
  }
}
