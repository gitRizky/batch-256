import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  //key - value
  SharedPreferencesHelper();

  static final String KEY_ISLOGIN = "KEY ISLOGIN";
  static final String KEY_USERNAME = "KEY USERNAME";
  static final String KEY_PASSWORD = "KEY PASSWORD";
  static final String KEY_ISREMEMBER = "KEY ISREMEMBER";
  static final String KEY_TOKEN = "KEY TOKEN";

  static Future<SharedPreferences> get sharedpreferences =>
      SharedPreferences.getInstance();
  //Untuk save token
  static Future saveToken(String token) async {
    final pref = await sharedpreferences;
    return pref.setString(KEY_TOKEN, token);
  }

  static Future<String> readToken() async {
    final pref = await sharedpreferences;
    return pref.getString(KEY_TOKEN) ?? "";
  }

  //untuk simpan flag isremember
  static Future saveIsRemember(bool isremember) async {
    final pref = await sharedpreferences;
    return pref.setBool(KEY_ISREMEMBER, isremember);
  }

  //untuk panggil remember
  static Future<bool> readIsRemember() async {
    final pref = await sharedpreferences;
    return pref.getBool(KEY_ISREMEMBER) ?? false;
  }

  //untuk simpan islogin (flag)
  static Future saveIsLogin(bool islogin) async {
    final pref = await sharedpreferences;
    return pref.setBool(KEY_ISLOGIN, islogin);
  }

  //untuk panggil islogin
  static Future<bool> readIsLogin() async {
    final pref = await sharedpreferences;
    return pref.getBool(KEY_ISLOGIN) ?? false;
  }

  //untuk username
  static Future saveUsername(String username) async {
    final pref = await sharedpreferences;
    return pref.setString(KEY_USERNAME, username);
  }

//untuk panggil username
  static Future<String> readUsername() async {
    final pref = await sharedpreferences;
    return pref.getString(KEY_USERNAME) ?? "";
  }

  //untuk password
  static Future savePassword(String password) async {
    final pref = await sharedpreferences;
    return pref.setString(KEY_PASSWORD, password);
  }

//untuk panggil password
  static Future<String> readPasword() async {
    final pref = await sharedpreferences;
    return pref.getString(KEY_PASSWORD) ?? "";
  }

  //clear semua data yang disimpan
  static Future clearAllData() async {
    final pref = await sharedpreferences;
    await Future.wait(<Future>[
      pref.setBool(KEY_ISLOGIN, false),
      pref.setString(KEY_USERNAME, ""),
      pref.setString(KEY_PASSWORD, ""),
      pref.setString(KEY_TOKEN, ""),
    ]);
  }
}
