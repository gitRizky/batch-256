import 'package:batch_256/utilities/networking/networking_response.dart';
import 'package:http/http.dart' as http;

class NetworkingConnetor {
  //GET request
  Future<NetworkingResponse> getRequest(
      String url,
      String path,
      Map<String, String> headerRequest,
      Map<String, String> queryParameter) async {
    try {
      var urlGET = url.indexOf("https") != -1
          ? Uri.https(
              url.substring(url.indexOf("//") + 2), path, queryParameter)
          : Uri.http(
              url.substring(url.indexOf("//") + 2), path, queryParameter);

      var response = await http.get(urlGET, headers: headerRequest);
      print("NetworkingResponse : " + response.body);
      return NetworkingSuccess(response.body, response.statusCode);
    } catch (exception) {
      print("NetworkingExeption : " + exception.toString());
      return NetworkingExceptions(exception.toString());
    }
  }

  Future<NetworkingResponse> postRequest(
      String url, Map<String, String> headerRequest, String bodyRequest) async {
    try {
      var response = await http.post(Uri.parse(url),
          headers: headerRequest, body: bodyRequest);
      print("NetworkingResponse : " + response.body);
      return NetworkingSuccess(response.body, response.statusCode);
    } catch (exception) {
      print("NetworkingExeption : " + exception.toString());
      return NetworkingExceptions(exception.toString());
    }
  }
}
