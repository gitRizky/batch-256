class NetworkingResponse {}

//jika problem di networking
class NetworkingExceptions extends NetworkingResponse {
  String message;

  NetworkingExceptions(this.message);
}

//jika sukses
class NetworkingSuccess extends NetworkingResponse {
  String body;
  int statusCode;

  NetworkingSuccess(this.body, this.statusCode);
}
