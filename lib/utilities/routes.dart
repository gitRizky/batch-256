class Routes {
  static const String splashscreen = '/splashscreen';
  static const String loginscreen = '/loginscreen';
  static const String registerscreen = '/registerscreen';
  static const String dashboardscreen = '/dashboardscreen';
  static const String parsingdatascreen = '/menus/parsingdatascreen';
  static const String detailuserscreen = '/menus/details/detailuserscreen';
  static const String loginscreenreqres = '/loginscreenreqres';
  static const String imagesliderscreen = '/menus/imagesliderscreen';
  static const String detailimagesliderscreen =
      '/menus/details/detailimagesliderscreen';
  static const String loginscreenplugin = '/loginscreenplugin';
  static const String cameragalleryscreen = '/menus/cameragalleryscreen';
  static const String locationservicesscreen = '/menus/locationservicesscreen';
  static const String mapservicesscreen = '/menus/mapservicesscreen';
  static const String listmahasiswascreeen = '/menus/listmahasiswascreeen';
  static const String inputdatamahasiswascreen =
      '/menus/inputdatamahasiswascreen';
  static const String detailmahasiswascreen = '/menus/detailmahasiswascreen';
  static const String updatedatamahasiswascreen =
      '/menus/updatedatamahasiswascreen';
}
