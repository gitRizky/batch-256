const API_BASE_URL = "https://reqres.in";
const END_POINT_LIST_USER = "/api/users";
const END_POINT_CREATE_USER = "/api/users";
const END_POINT_LOGIN_USER = "/api/login";
