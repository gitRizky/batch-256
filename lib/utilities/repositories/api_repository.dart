import 'dart:convert';

import 'package:batch_256/utilities/constants.dart';
import 'package:batch_256/utilities/networking/networking_connector.dart';
import 'package:batch_256/utilities/networking/networking_response.dart';

class APIRepository {
  //semua request yg berhubungan API

  //API Untuk get list user
  Future<NetworkingResponse> getListUser(int page) {
    String url = API_BASE_URL;
    Map<String, String> headerRequest = {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    };

    Map<String, String> queryParameter = {'page': "$page"};
    return NetworkingConnetor()
        .getRequest(url, END_POINT_LIST_USER, headerRequest, queryParameter);
  }

  //API untuk create user
  Future<NetworkingResponse> createUser(String name, String job) {
    String url = API_BASE_URL + END_POINT_CREATE_USER;

    Map<String, String> headerRequest = {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    };

    final bodyRequest = jsonEncode({"name": name, "job": job});

    return NetworkingConnetor().postRequest(url, headerRequest, bodyRequest);
  }

  //API LOGIN USER
  Future<NetworkingResponse> loginUser(String email, String password) {
    String url = API_BASE_URL + END_POINT_LOGIN_USER;
    Map<String, String> headerRequest = {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json'
    };
    final bodyRequest = jsonEncode({"email": email, "password": password});
    return NetworkingConnetor().postRequest(url, headerRequest, bodyRequest);
  }
}
