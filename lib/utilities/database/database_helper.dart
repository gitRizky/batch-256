import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DataBaseHelper {
  String dataBaseName = 'info_mahasiswa.db';
  String tableName = "profil_mahasiswa";

  //init database
  Future<Database> initDatabase() async {
    return openDatabase(join(await getDatabasesPath(), '$dataBaseName'),
        //versi database, default = 1, jadi apabila ada update, naikkan versinya
        version: 1,
        //operasi yg dilakukan jika terkoneksi dengan db pertama kali
        onCreate: createTable);
  }

  //create table jika belum ada
  void createTable(Database db, int version) async {
    String syntaxQuery = '''CREATE TABLE "$tableName" (
        "id"	INTEGER,
        "nama"	TEXT,
        "tanggal_lahir"	TEXT,
        "gender"	TEXT,
        "alamat"	TEXT,
        "jurusan"	TEXT,
        "foto_path"	TEXT,
        PRIMARY KEY("id" AUTOINCREMENT));''';

    await db.execute(syntaxQuery);
  }
}
