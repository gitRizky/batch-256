import 'package:batch_256/models/info_mahasiswa_model.dart';
import 'package:batch_256/utilities/repositories/db_repository.dart';

class InfoMahasiswaViewModel {
  InfoMahasiswaViewModel();

  Future<int> insertDataMahasiswa(InfoMahasiswaModel model) async {
    return await DbRepository().inserDataMahasiswa(model);
  }

  Future<List<InfoMahasiswaModel>> readDataMahasiswa() async {
    return await DbRepository().readDataMahasiswa();
  }

  Future<int> updateDataMahasiswa(InfoMahasiswaModel model) async {
    return await DbRepository().updateDataMahasiswa(model);
  }

  Future<int> deletedataMahasiswa(InfoMahasiswaModel model) async {
    return await DbRepository().deleteDataMahasiswa(model);
  }

  Future<List<InfoMahasiswaModel>> searchDataMahasiswa(String keyword) async {
    return await DbRepository().searchDataMahasiswa(keyword);
  }
}
