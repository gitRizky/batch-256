import 'package:batch_256/components/custom_snackbar.dart';
import 'package:batch_256/models/list_user_model.dart';
import 'package:batch_256/models/response_unsuccess.dart';
import 'package:batch_256/utilities/networking/networking_response.dart';
import 'package:batch_256/utilities/repositories/api_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListUsersViewModel {
  ListUsersViewModel();

  Future<ListUserModel> getDataFromAPI(BuildContext context, int page) async {
    NetworkingResponse response = await APIRepository().getListUser(page);

    if (response is NetworkingSuccess) {
      if (response.statusCode == 200) {
        //prose data -> jadikan model
        ListUserModel model = listUserModelFromJson(response.body);
        return model;
      } else {
        print('Status Code : ${response.statusCode}');
        print("Message : ${response.body}");

        ResponseUnsuccess unsuccess = responseUnsuccessFromJson(response.body);
        String message =
            unsuccess.message + " [" + response.statusCode.toString() + "]";

        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(message));
        return null;
      }
    } else if (response is NetworkingExceptions) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar(response.message));
      return null;
    }
  }
}
