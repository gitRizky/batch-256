import 'package:batch_256/components/custom_snackbar.dart';
import 'package:batch_256/models/create_user_model.dart';
import 'package:batch_256/models/response_unsuccess.dart';
import 'package:batch_256/utilities/networking/networking_response.dart';
import 'package:batch_256/utilities/repositories/api_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreateUserViewModel {
  CreateUserViewModel();

  Future<CreateUserModel> postDataToAPI(
      BuildContext context, String name, String job) async {
    NetworkingResponse response = await APIRepository().createUser(name, job);
    if (response is NetworkingSuccess) {
      if (response.statusCode == 201) {
        CreateUserModel model = createUserModelFromJson(response.body);
        return model;
      } else {
        print("HTTP Status Code : ${response.statusCode}");
        print("Message : ${response.body}");

        ResponseUnsuccess responseUnsuccess =
            responseUnsuccessFromJson(response.body);

        String message = responseUnsuccess.message + "[${response.statusCode}]";
        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(message));
        return null;
      }
    } else if (response is NetworkingExceptions) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar(response.message));
      return null;
    }
  }
}
