import 'dart:convert';

import 'package:batch_256/components/custom_snackbar.dart';
import 'package:batch_256/models/login_model.dart';
import 'package:batch_256/models/response_unsuccess.dart';
import 'package:batch_256/utilities/networking/networking_response.dart';
import 'package:batch_256/utilities/repositories/api_repository.dart';
import 'package:batch_256/views/loginscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginUserViewModel {
  LoginUserViewModel();
  Future<LoginModel> postDataToAPI(
      BuildContext context, String email, String password) async {
    NetworkingResponse response =
        await APIRepository().loginUser(email, password);
    if (response is NetworkingSuccess) {
      if (response.statusCode == 200) {
        LoginModel model = loginModelFromJson(response.body);
        return model;
      } else if (response.statusCode == 400) {
        print("Status code : ${response.statusCode}");
        print("Status code : ${response.body}");
        var error = jsonDecode(response.body);
        ScaffoldMessenger.of(context)
            .showSnackBar(customSnackBar(error["error"]));
        return null;
      } else {
        print("Status code : ${response.statusCode}");
        print("Status code : ${response.body}");
        ResponseUnsuccess responseUnsuccess =
            responseUnsuccessFromJson(response.body);
        String message = responseUnsuccess.message + "[${response.statusCode}]";
        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(message));
        return null;
      }
    } else if (response is NetworkingExceptions) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar(response.message));
    }
  }
}
