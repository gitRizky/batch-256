import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterScreenState();
}

class RegisterScreenState extends State<StatefulWidget> {
  String tanggalLahir = '-';
  DateTime tanggalTerpilih = DateTime.now();
  bool agreeValue = false;
  int genderRadioValue = 0;
  String valuePekerjaan;
  List itemPekerjaan = [
    'Designer',
    'Programmer',
    'Business Analyst',
    'Software Tester'
  ];
  String valuePendidikan;
  List itemPendidikan = ['SD', 'SMP', 'SLTA', 'D3', 'S1', 'S2', 'S3'];
  String valueHobi;
  List itemHobi = ['Membaca', 'Berenang'];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      color: Colors.lightBlue[50],
      child: Scaffold(
        appBar: AppBar(
          title: Text("Registration Form"),
        ),
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: ListView(
            padding: const EdgeInsets.all(10),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Nama Lengkap Anda",
                    hintStyle: TextStyle(color: Colors.black12),
                    counterText: "",
                  ),
                  onChanged: (value) {},
                  maxLength: 50,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  keyboardType: TextInputType.number,
                  maxLength: 5,
                  decoration: InputDecoration(
                    hintText: "Usia Anda",
                    hintStyle: TextStyle(color: Colors.black12),
                    counterText: "",
                  ),
                  onChanged: (value) {},
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Alamat",
                    hintStyle: TextStyle(color: Colors.black12),
                    counterText: "",
                  ),
                  onChanged: (value) {},
                  maxLength: 120,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    hintText: "Nomor Telpon",
                    hintStyle: TextStyle(color: Colors.black12),
                    counterText: "",
                  ),
                  onChanged: (value) {},
                  maxLength: 16,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: "Email",
                    hintStyle: TextStyle(color: Colors.black12),
                    counterText: "",
                  ),
                  onChanged: (value) {},
                  maxLength: 30,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Jenis Kelamin"),
                    Row(children: [
                      new Radio(
                        value: 0,
                        groupValue: genderRadioValue,
                        onChanged: handleRadioClick,
                      ),
                      Text("Pria"),
                      new Radio(
                        value: 1,
                        groupValue: genderRadioValue,
                        onChanged: handleRadioClick,
                      ),
                      Text("Wanita")
                    ]),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Jenis Pekerjaan"),
                    Container(
                      width: size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                        //color: Colors.grey,
                      ),
                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton(
                          hint: Text('-silahkan pilih'),
                          value: valuePekerjaan,
                          items: itemPekerjaan.map((value) {
                            return DropdownMenuItem(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              valuePekerjaan = value;
                            });
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Pendidikan"),
                    Container(
                      width: size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton(
                          hint: Text('-silahkan pilih'),
                          value: valuePendidikan,
                          items: itemPendidikan.map((value) {
                            return DropdownMenuItem(
                              child: Text(value),
                              value: value,
                            );
                          }).toList(),
                          onChanged: (pendidikan) {
                            setState(() {
                              valuePendidikan = pendidikan;
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Hobi"),
                    DropdownButton(
                      hint: Text('-silahkan pilih'),
                      value: valueHobi,
                      items: itemHobi.map((value) {
                        return DropdownMenuItem(
                          child: Text(value),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (hobi) {
                        setState(() {
                          valueHobi = hobi;
                        });
                      },
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Pilih Tanggal Lahir'),
                    Row(
                      children: [
                        IconButton(
                            icon: Icon(Icons.calendar_today),
                            onPressed: () {
                              pilihTanggalLahir(context);
                            }),
                        Text(
                          '$tanggalLahir',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 30,
                ),
                child: Text(
                    "Dengan registrasi anda telah menyetujui segala syarat dan ketentuan yang berlaku. "
                    "Silahkan centang jika anda setuju"),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 10,
                ),
                child: CheckboxListTile(
                  title: Text('Saya setuju'),
                  value: agreeValue,
                  onChanged: (newValue) {
                    setState(() {
                      agreeValue = newValue;
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              ElevatedButton(
                //Ternary             if                  else
                onPressed: agreeValue ? () => sayaSetuju() : null,
                child: Text('Register'),
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }

  sayaSetuju() {
    //validasi data
    Navigator.of(context).pop();
  }

  void pilihTanggalLahir(BuildContext _context) async {
    final DateTime picker = await showDatePicker(
        context: _context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (picker != null && picker != tanggalTerpilih) {
      setState(() {
        tanggalTerpilih = picker;

        tanggalLahir = DateFormat('dd-MMM-yyyy').format(tanggalTerpilih);
      });
    }
  }

  void handleRadioClick(int value) {
    setState(() {
      genderRadioValue = value;

      switch (genderRadioValue) {
        case 0:
          Fluttertoast.showToast(
              msg: "Anda memilih gender Pria", toastLength: Toast.LENGTH_SHORT);
          //gravity: ToastGravity.CENTER);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "Anda memilih gender Wanita",
              toastLength: Toast.LENGTH_SHORT);
          //gravity: ToastGravity.CENTER);
          break;
      }
    });
  }
}
