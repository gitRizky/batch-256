import 'package:batch_256/utilities/routes.dart';
import 'package:batch_256/utilities/shared_preferences.dart';
import 'package:batch_256/views/loginscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'navigation_drawer.dart';

class DashboardScreen extends StatefulWidget {
  @override
  DashboardScreenState createState() => DashboardScreenState();
}

class DashboardScreenState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Dashboard Menu'),
      ),
      drawer: NavigationDrawer(),
      body: DashboardScreenLayout(),
    );
  }
}

class DashboardScreenLayout extends StatefulWidget {
  @override
  DashboardScreenLayoutState createState() => DashboardScreenLayoutState();
}

class DashboardScreenLayoutState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: [
        Card(
          color: Colors.red,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.archivebox,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Parsing Data',
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.parsingdatascreen));
            },
          ),
        ),
        Card(
          color: Colors.orange,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.photo,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Imaging and Slider',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.imagesliderscreen));
            },
          ),
        ),
        Card(
          color: Colors.green,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.camera,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Camera & Gallery',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.cameragalleryscreen));
            },
          ),
        ),
        Card(
          color: Colors.lime,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.location,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Location Services',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.locationservicesscreen));
            },
          ),
        ),
        Card(
          color: Colors.purple,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.doc,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Database',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.listmahasiswascreeen));
            },
          ),
        ),
        Card(
          color: Colors.blue,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.map_pin_ellipse,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'Map Services',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, (Routes.mapservicesscreen));
            },
          ),
        ),
        Card(
          color: Colors.indigo,
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  CupertinoIcons.compass,
                  color: Colors.white,
                  size: 75.0,
                ),
                Text(
                  'State Management',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )
              ],
            ),
            onTap: () {},
          ),
        )
      ],
    );
  }
}
// class DashboardScreenState extends State<DashboardScreen> {
//   String username = "";
//   String password = "";
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Scaffold(
//         body: GestureDetector(
//           child: Row(
//             children: [
//               SizedBox(
//                 height: 100,
//               ),
//               Text("username = $username, password = $password"),
//               TextButton(
//                   onPressed: () {
//                     //flag is loagin jadikan false
//                     SharedPrefrencesHelper.saveIsLogin(false);
//                     //buang screen dashboard, tampilkan login
//                     Navigator.of(context)
//                         .pushReplacement(MaterialPageRoute(builder: (_) {
//                       return LoginScreen();
//                     }));
//                   },
//                   child: Text('Logout'))
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   @override
//   void initState() {
//     super.initState();
//
//     SharedPrefrencesHelper.readUsername().then((value) {
//       setState(() {
//         username = value;
//       });
//     });
//     SharedPrefrencesHelper.readPasword().then((value) {
//       setState(() {
//         password = value;
//       });
//     });
//
//     //SharedPrefrencesHelper.clearAllData();
//   }
// }
