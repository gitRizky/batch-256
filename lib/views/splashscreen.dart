import 'dart:async';

import 'package:batch_256/utilities/shared_preferences.dart';
import 'package:batch_256/views/dashboardscreen.dart';
import 'package:batch_256/views/loginscreenplugin.dart';
import 'package:batch_256/views/loginscreenreqres.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'loginscreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    //buat layout disini
    return Scaffold(
      backgroundColor: Colors.green,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/logo_xa.png",
              width: 150.0,
              height: 150.0,
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20, bottom: 20, left: 10, right: 10)),
            Text(
              "Batch 256 Flutter",
              style: TextStyle(
                  color: Color(0XFFFFFFFF),
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal,
                  fontFamily: "BiomeRegular"),
            ),
            SizedBox(
              height: 10.0,
            ),
            RichText(
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                    text: "Versi",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontFamily: "BiomeRegular")),
                TextSpan(
                    text: "1.0",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontStyle: FontStyle.italic,
                        fontFamily: "BiomeRegular"))
              ]),
            )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    startSplashScreenDelay();
  }

  startSplashScreenDelay() async {
    var duration = Duration(seconds: 3);
    Timer(duration, callback);
  }

  void callback() {
    //logic apakah sudah login
    SharedPreferencesHelper.readIsLogin().then((islogin) {
      if (islogin) {
        //sudah login
        pindahKeDashboardScreen();
      } else {
        pindahKeLoginScreen();
      }
    });
  }

  void pindahKeLoginScreen() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
      return LoginScreen();
      //LoginScreenReqres();
      //LoginScreenPlugin();
    }));
  }

  void pindahKeDashboardScreen() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
      return DashboardScreen();
    }));
  }
}
