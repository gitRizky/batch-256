import 'file:///C:/Users/XSIS%20Bootcamp/AndroidStudioProjects/batch_256/lib/components/custom_alert_dialog.dart';
import 'package:batch_256/utilities/shared_preferences.dart';
import 'package:batch_256/views/dashboardscreen.dart';
import 'package:batch_256/views/registerscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  bool isRememberMe = false;
  String username = "";
  String password = "";
  bool _showPassword = false;

  final TextEditingController _controllerUsername = new TextEditingController();
  final TextEditingController _controllerPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //mengetahui ukuran layar
    Size size = MediaQuery.of(context).size;

    return Container(
      color: Colors.white,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          onTap: () {
            //Menghilangkan Keyboard
            FocusScope.of(context).unfocus();
          },
          child: Container(
            width: size.width,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50.0,
                  ),
                  Image.asset(
                    "assets/images/logo_xa.png",
                    width: 100.0,
                    height: 100.0,
                  ),
                  Text(
                    "Login Form",
                    style: TextStyle(
                        color: Color(0XFF000000),
                        fontSize: 22,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: TextField(
                        controller: _controllerUsername,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: "Input Username",
                            hintStyle: TextStyle(color: Colors.grey)),
                        onChanged: (value) {
                          username = value;
                        },
                      )),
                  SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: TextField(
                        controller: _controllerPassword,
                        obscureText: !_showPassword,
                        onChanged: (value) {
                          password = value;
                        },
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            onTap: () {
                              _togglevisibility();
                            },
                            child: Icon(_showPassword
                                ? Icons.visibility
                                : Icons.visibility_off),
                          ),
                          border: OutlineInputBorder(),
                          hintText: "Input Password",
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                    child: CheckboxListTile(
                      title: Text("Remember Me"),
                      value: isRememberMe,
                      onChanged: (newValue) {
                        setState(() {
                          isRememberMe = newValue;
                        });
                      },
                      controlAffinity: ListTileControlAffinity.leading,
                    ),
                  ),
                  TextButton(
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                      /*overlayColor: MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.focused))
                            return Colors.lightBlue;
                          if (states.contains(MaterialState.hovered))
                            return Colors.blueAccent;
                          if (states.contains(MaterialState.pressed))
                            return Colors.blue[50];
                          return null;
                        })*/
                    ),
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      validateLogin(context);
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    child: Text(
                      "Register New User",
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      pindahKeRegisterScreen(context);
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void validateLogin(BuildContext _context) {
    if (username.isEmpty) {
      //belum input
      showAlert(_context, "Warning", "Anda belum mengisi username!");
    } else if (password.isEmpty) {
      showAlert(_context, "Warning", "Anda belum mengisi password!");
    } else {
      SharedPreferencesHelper.saveIsLogin(true);
      SharedPreferencesHelper.saveUsername(username);
      SharedPreferencesHelper.savePassword(password);
      SharedPreferencesHelper.saveIsRemember(isRememberMe);
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return DashboardScreen();
      }));
    }
  }

  void showAlert(BuildContext _context, String _title, String _message) {
    //belum input
    var alertDialog = CustomAlertDialog(
      title: _title,
      message: _message,
      action_text: "Ok",
    );
    showDialog(context: _context, builder: (_) => alertDialog);
  }

  void pindahKeRegisterScreen(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return RegisterScreen();
    }));
  }

  @override
  void initState() {
    super.initState();
    SharedPreferencesHelper.readIsRemember().then((value) {
      if (value) {
        SharedPreferencesHelper.readUsername().then((value) {
          setState(() {
            username = value;
            _controllerUsername.text = value;
          });
        });
        SharedPreferencesHelper.readUsername().then((value) {
          setState(() {
            password = value;
            _controllerPassword.text = value;
          });
        });
      }
    });
  }

  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }
}
