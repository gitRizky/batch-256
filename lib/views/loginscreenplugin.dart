import 'package:batch_256/utilities/shared_preferences.dart';
import 'package:batch_256/viewmodel/login_user_viewmodel.dart';
import 'package:batch_256/views/dashboardscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';

class LoginScreenPlugin extends StatelessWidget {
  BuildContext _context;
  LoginData _loginData;
  @override
  Widget build(BuildContext context) {
    _context = context;
    return FlutterLogin(
      title: 'Flutter Batch 256',
      logo: 'assets/images/logo_xa.png',
      onLogin: _authUserReqres,
      onSubmitAnimationCompleted: () {
        loginUser(_loginData.name, _loginData.password);
        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        //   return DashboardScreen();
        // }));
      },
    );
  }

  Duration get loginTime => Duration(microseconds: 3000);
  static const users = const {
    'dribble@gmail.com': '12345',
  };

  Future<String> _authUser(LoginData data) {
    print("Name: ${data.name}, Password: ${data.password}");
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.name)) {
        return "Username not exists";
      }
      if (users[data.name] != data.password) {
        return "Password does not match";
      }
      return null;
    });
  }

  Future<String> _authUserReqres(LoginData data) async {
    print('Name: ${data.name}, Password ${data.password}');
    _loginData = data;
    return null;
  }

  void loginUser(String email, String password) async {
    await LoginUserViewModel()
        .postDataToAPI(_context, email, password)
        .then((value) {
      if (value != null) {
        String token = value.token;
        SharedPreferencesHelper.saveToken(token);
        SharedPreferencesHelper.saveIsLogin(true);
        SharedPreferencesHelper.saveUsername(email);
        SharedPreferencesHelper.savePassword(password);
        SharedPreferencesHelper.saveIsRemember(false);
        Navigator.of(_context).pushReplacement(MaterialPageRoute(builder: (_) {
          return DashboardScreen();
        }));
      } else {
        Navigator.of(_context).pushReplacement(MaterialPageRoute(builder: (_) {
          return LoginScreenPlugin();
        }));
      }
    });
  }
}
