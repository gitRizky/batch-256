import 'file:///C:/Users/XSIS%20Bootcamp/AndroidStudioProjects/batch_256/lib/components/costum_confirmation_dialog.dart';
import 'package:batch_256/utilities/routes.dart';
import 'package:batch_256/utilities/shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:batch_256/components/custom_expansion_tile.dart' as custom;

import 'loginscreen.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  NavigationDrawerState createState() => NavigationDrawerState();
}

class NavigationDrawerState extends State<StatefulWidget> {
  String username = "";
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: [createHeaderMenu(context), createContentMenu(context)],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
            child: TextButton(
              onPressed: () {
                var confirmationDialog = CustomConfirmationDialog(
                  title: 'Confirmation',
                  message: "Apakah anda yakin ingin logout",
                  yes: 'Yes',
                  no: 'No',
                  pressYes: () {
                    SharedPreferencesHelper.saveIsLogin(false);
                    Navigator.of(context).pop();
                    // Navigator.pushReplacementNamed(context, Routes.loginscreen);
                    Navigator.pushReplacementNamed(
                        context,
                        // Routes.loginscreenreqres
                        Routes.loginscreen);
                  },
                  pressNo: () {
                    //tidak jadi logout
                    Navigator.of(context).pop();
                  },
                );
                showDialog(
                    context: context, builder: (_) => confirmationDialog);
              },
              child: Text(
                'Logout',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red)),
            ),
          )
        ],
      ),
    );
  }

  Widget createHeaderMenu(BuildContext context) {
    return Container(
      //color: Colors.black12,
      height: MediaQuery.of(context).size.height * 0.3,
      child: DrawerHeader(
        decoration: BoxDecoration(
          color: Colors.green,
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(360.0),
                  child: Image.network(
                    "https://image.flaticon.com/icons/png/512/149/149071.png",
                    height: 50.0,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                // Expanded(
                //   child: Text(
                //     username,
                //     style: TextStyle(
                //         color: Colors.white,
                //         fontSize: 22,
                //         fontWeight: FontWeight.bold),
                //     overflow: TextOverflow.ellipsis,
                //   ),
                // )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  username,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            // Text(
            //   "Versi 1.0",
            //   style: TextStyle(color: Colors.white, fontSize: 12),
            // )
          ],
        ),
      ),
    );
  }

  Widget createContentMenu(BuildContext context) {
    return Column(
      //color: Colors.white,
      // height: MediaQuery.of(context).size.height,
      children: [
        Card(
          margin: EdgeInsets.fromLTRB(5, 3, 5, 3),
          color: Colors.orange,
          child: ListTile(
            title: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    CupertinoIcons.home,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    'Dashboard',
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, Routes.dashboardscreen);
            },
          ),
        ),
        Card(
          margin: EdgeInsets.fromLTRB(5, 3, 5, 3),
          child: custom.ExpansionTile(
            headerBackgroundColor: Colors.orange,
            iconColor: Colors.white,
            leading: Icon(
              CupertinoIcons.gear_solid,
              color: Colors.white,
            ),
            title: Transform(
              transform: Matrix4.translationValues(-10, 0, 0),
              child: Text(
                "Administrator",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
            children: [
              ListTile(
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.minus,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Daftar Pengguna",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
              ListTile(
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.minus,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Edit Password",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
              ListTile(
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.minus,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Laporan Bulanan",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Card(
          margin: EdgeInsets.fromLTRB(5, 3, 5, 3),
          child: custom.ExpansionTile(
            headerBackgroundColor: Colors.orange,
            iconColor: Colors.white,
            leading: Icon(
              CupertinoIcons.gear_solid,
              color: Colors.white,
            ),
            title: Transform(
              transform: Matrix4.translationValues(-10, 0, 0),
              child: Text(
                "Settings",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
            ),
            children: [
              ListTile(
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.minus,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Pengaturan Bahasa",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
              ListTile(
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.minus,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Pengaturan Theme",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    SharedPreferencesHelper.readUsername().then((value) {
      setState(() {
        username = value;
      });
    });
  }
}
