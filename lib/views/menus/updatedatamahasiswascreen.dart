import 'package:batch_256/components/custom_snackbar.dart';
import 'package:batch_256/models/info_mahasiswa_model.dart';
import 'package:batch_256/viewmodel/info_mahasiswa_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:intl/intl.dart';

class UpdateDataMahasiswaScreen extends StatefulWidget {
  InfoMahasiswaModel data;
  UpdateDataMahasiswaScreen({this.data});
  @override
  UpdateDataMahasiswaScreenState createState() =>
      UpdateDataMahasiswaScreenState();
}

class UpdateDataMahasiswaScreenState extends State<UpdateDataMahasiswaScreen> {
  TextEditingController _nama = new TextEditingController();
  TextEditingController _gender = new TextEditingController();
  TextEditingController _alamat = new TextEditingController();
  TextEditingController _jurusan = new TextEditingController();
  String tanggal_lahir = "";
  String foto_path = "";
  File _image;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _nama.text = widget.data.nama;
      _gender.text = widget.data.gender;
      _alamat.text = widget.data.alamat;
      _jurusan.text = widget.data.jurusan;
      tanggal_lahir = widget.data.tanggal_lahir;
      foto_path = widget.data.foto_path;

      _image = File(foto_path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Input Data"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                maxLength: 50,
                decoration: InputDecoration(hintText: "Nama Lengkap"),
                controller: _nama,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Pilih Tanggal Lahir'),
                  Row(
                    children: [
                      IconButton(
                          icon: Icon(Icons.calendar_today),
                          onPressed: () {
                            pilihTanggalLahir();
                          }),
                      Text(
                        '$tanggal_lahir',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                maxLength: 1,
                decoration: InputDecoration(hintText: "Jenis Kelamin (L/P)"),
                controller: _gender,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                maxLength: 200,
                decoration: InputDecoration(hintText: "Alamat"),
                controller: _alamat,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                maxLength: 30,
                decoration: InputDecoration(hintText: "Jurusan"),
                controller: _jurusan,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 40),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: GestureDetector(
                  onTap: () {
                    getImageFromCamera();
                  },
                  child: _image == null
                      ? Image.asset(
                          "assets/images/logo_xa.png",
                          width: 100,
                          height: 100,
                        )
                      : Image.file(
                          _image,
                          width: 100,
                          height: 100,
                        ),
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                validasiInput();
              },
              child: Text(
                "Update",
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue)),
            )
          ],
        ),
      ),
    );
  }

  void validasiInput() {
    if (_nama.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Nama harus diisi"));
    } else if (tanggal_lahir.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Tanggal Lahir harus diisi"));
    } else if (_gender.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Gender harus diisi"));
    } else if (_alamat.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Alamat harus diisi"));
    } else if (_jurusan.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Jurusan harus diisi"));
    } else if (_image == null || foto_path.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Foto profil belum diisi"));
    } else {
      //simpan data ke database
      updateDataMahasiswa();
    }
  }

  void updateDataMahasiswa() async {
    InfoMahasiswaModel model = new InfoMahasiswaModel(
        id: widget.data.id,
        nama: _nama.text,
        tanggal_lahir: tanggal_lahir,
        gender: _gender.text,
        alamat: _alamat.text,
        jurusan: _jurusan.text,
        foto_path: foto_path);
    await InfoMahasiswaViewModel().updateDataMahasiswa(model).then((value) {
      print('Respon Database : $value');
      Navigator.pop(context);
    });
  }

  pilihTanggalLahir() async {
    final DateTime picker = await showDatePicker(
        context: (context),
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime.now());
    if (picker != null) {
      setState(() {
        tanggal_lahir = DateFormat('dd/MM/yyyy').format(picker);
      });
    }
  }

  Future getImageFromCamera() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) _image = File(pickedFile.path);
      foto_path = pickedFile.path;
    });
  }
}
