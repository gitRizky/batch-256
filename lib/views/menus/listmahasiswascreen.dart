import 'package:batch_256/components/costum_confirmation_dialog.dart';
import 'package:batch_256/models/info_mahasiswa_model.dart';
import 'package:batch_256/utilities/routes.dart';
import 'package:batch_256/viewmodel/info_mahasiswa_viewmodel.dart';
import 'package:batch_256/views/menus/details/detailmahasiswascreen.dart';
import 'package:batch_256/views/menus/updatedatamahasiswascreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class ListMahasiswaScreen extends StatefulWidget {
  @override
  ListMahasiswaScreenState createState() => ListMahasiswaScreenState();
}

class ListMahasiswaScreenState extends State<StatefulWidget> {
  List<InfoMahasiswaModel> _listMahasiswa = [];

  TextEditingController _keywordPencarian = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tampilkanListDataMahasiswa();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Database Mahasiswa"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Center(
            child: Column(
              children: [
                Text(
                  "List Mahasiswa",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                  child: TextField(
                    controller: _keywordPencarian,
                    decoration: InputDecoration(
                      hintText: "Keyword Pencarian",
                      suffixIcon: IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () {
                          cariDataMahasiswa();
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: _listMahasiswa.length == 0
                ? Text("Data Tidak Ditemukan")
                : ListView.builder(
                    itemCount: _listMahasiswa.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: InkWell(
                          child: Card(
                            color:
                                index % 2 == 0 ? Colors.white : Colors.white24,
                            margin: EdgeInsets.all(5),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(5),
                                  child: CircleAvatar(
                                    radius: 40,
                                    child: Image.file(
                                      File(_listMahasiswa[index].foto_path),
                                      // width: 30,
                                      // height: 30,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        _listMahasiswa[index].nama,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        'Jurusan ${_listMahasiswa[index].jurusan}',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Row(
                                        children: [
                                          IconButton(
                                            icon: Icon(Icons.edit),
                                            onPressed: () {
                                              InfoMahasiswaModel data =
                                                  _listMahasiswa[index];
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          UpdateDataMahasiswaScreen(
                                                            data: data,
                                                          ))).then((value) {
                                                tampilkanListDataMahasiswa();
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          IconButton(
                                            icon: Icon(Icons.delete_forever),
                                            onPressed: () {
                                              //untuk delete adata
                                              var confirmationDialog =
                                                  CustomConfirmationDialog(
                                                title: "Hapus Data",
                                                message:
                                                    "Yakin ingin menghapus data?",
                                                yes: "ya",
                                                no: "tidak",
                                                pressYes: () {
                                                  InfoMahasiswaModel data =
                                                      _listMahasiswa[index];
                                                  hapusDataMahasiswa(data);
                                                  Navigator.of(context).pop();
                                                },
                                                pressNo: () {
                                                  Navigator.of(context).pop();
                                                },
                                              );
                                              showDialog(
                                                  context: context,
                                                  builder: (_) =>
                                                      confirmationDialog);
                                            },
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            InfoMahasiswaModel data = _listMahasiswa[index];
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailMahasiswaScreen(
                                          data: data,
                                        )));
                          },
                        ),
                      );
                    }),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Tambah Data",
        onPressed: () {
          Navigator.of(context).pushNamed(Routes.inputdatamahasiswascreen)
              //tambah isi saaat back
              .then((value) {
            setState(() {
              tampilkanListDataMahasiswa();
            });
          });
        },
      ),
    );
  }

  tampilkanListDataMahasiswa() async {
    await InfoMahasiswaViewModel().readDataMahasiswa().then((value) {
      setState(() {
        print('Jumlah record ditemukan + ${value.length}');
        _listMahasiswa = value;
      });
    });
  }

  hapusDataMahasiswa(InfoMahasiswaModel data) async {
    await InfoMahasiswaViewModel().deletedataMahasiswa(data).then((value) {
      setState(() {
        tampilkanListDataMahasiswa();
      });
    });
  }

  cariDataMahasiswa() async {
    if (_keywordPencarian.text.isEmpty) {
      tampilkanListDataMahasiswa();
    } else {
      await InfoMahasiswaViewModel()
          .searchDataMahasiswa(_keywordPencarian.text)
          .then((value) {
        setState(() {
          _listMahasiswa = value;
        });
      });
    }
  }
}
