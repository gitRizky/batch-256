import 'package:batch_256/views/menus/details/detailimagescreen.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageSliderScreen extends StatefulWidget {
  @override
  ImageSliderScreenState createState() => ImageSliderScreenState();
}

class ImageSliderScreenState extends State<ImageSliderScreen> {
  final List<String> imageUrls = [
    'https://cdn.images.express.co.uk/img/dynamic/59/590x/Google-UK-Code-of-Conduct-Dont-Be-Evil-Alphabet-Company-Be-Evil-Alphabet-Google-vs-Alphabet-Code-of-Conduct-Employees-Manrta-610142.jpg',
    'https://img.unocero.com/2018/09/google-photos-images3-ss-1920-1.jpg',
    'https://img.unocero.com/2020/04/google-meet.jpg',
    'https://img.phonandroid.com/2020/09/google-play-store-2.jpg',
    'https://wallpapercave.com/wp/wp1918181.jpg'
  ];

  int _currentPosition = 0;
  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = imageUrls
        .map((item) => Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailImageScreen(
                                  urlImage: item,
                                )));
                  },
                  child: Stack(
                    children: [
                      Image.network(
                        item,
                        fit: BoxFit.cover,
                        width: 1000.0,
                      ),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Colors.white, Colors.transparent],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                          child: Text(
                            "Gambar ke ${imageUrls.indexOf(item)}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ))
        .toList();
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Image Slider'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CarouselSlider(
              options: CarouselOptions(
                  autoPlay: true,
                  aspectRatio: 2.0,
                  enlargeCenterPage: true,
                  enlargeStrategy: CenterPageEnlargeStrategy.height,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _currentPosition = index;
                    });
                  }),
              items: imageSliders,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: imageUrls.map((url) {
                int index = imageUrls.indexOf(url);
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentPosition == index
                          ? Colors.black
                          : Colors.black12),
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
