import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom/pinch_zoom.dart';

class DetailImageScreen extends StatelessWidget {
  final String urlImage;
  DetailImageScreen({this.urlImage});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PinchZoom Img'),
      ),
      body: PinchZoom(
        image: Image.network(this.urlImage),
        zoomedBackgroundColor: Colors.black.withOpacity(0.5),
        resetDuration: const Duration(microseconds: 100),
        maxScale: 2.5,
        onZoomStart: () {
          print('Img zoom in');
        },
        onZoomEnd: () {
          print('Img zoom out');
        },
      ),
    );
  }
}
