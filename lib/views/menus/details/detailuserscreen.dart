import 'package:batch_256/components/custom_snackbar.dart';
import 'package:batch_256/models/list_user_model.dart';
import 'package:batch_256/viewmodel/create_user_viewmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class DetailUserScreen extends StatefulWidget {
  //buat constructor untuk handle pasing data dari list
  final Datum data;
  DetailUserScreen({this.data});
  @override
  DetailUserScreenState createState() => DetailUserScreenState();
}

//karena ada custom constructor, maka extends state nya menggunakan <DetailUserScreen> agar data bisa dibaca
class DetailUserScreenState extends State<DetailUserScreen> {
  final TextEditingController _controllerName = new TextEditingController();
  final TextEditingController _controllerJob = new TextEditingController();

  ProgressDialog loading;

  @override
  Widget build(BuildContext context) {
    loading = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    loading.style(
        message: "mengirim data...",
        progressWidget: CircularProgressIndicator(),
        borderRadius: 10.0,
        elevation: 10.0,
        insetAnimCurve: Curves.bounceIn);
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Detail Screen"),
        ),
        resizeToAvoidBottomInset: false,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Center(
            //   child: Image.network(
            //     widget.data.avatar,
            //     width: 150,
            //     height: 150,
            //   ),
            // ),
            //versi cached
            Center(
              child: CachedNetworkImage(
                imageUrl: widget.data.avatar,
                width: 150,
                height: 150,
                fit: BoxFit.cover,
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "${widget.data.firstName} ${widget.data.lastName}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "${widget.data.email}",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            ),
            Divider(
              color: Colors.black12,
              height: 5.0,
              thickness: 5.0,
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                decoration: InputDecoration(hintText: "Input Name"),
                controller: _controllerName,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                decoration: InputDecoration(hintText: "Input Job"),
                controller: _controllerJob,
              ),
            ),
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                validateInput(
                    _controllerName.text.trim(), _controllerJob.text.trim());
              },
              child: Text(
                "Create User",
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue)),
            ),
          ],
        ),
      ),
    );
  }

  void validateInput(String name, String job) {
    if (name.isNotEmpty && job.isNotEmpty) {
      //kriim ke API
      createUser(name, job);
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Semua Inputan Harus Diisi"));
    }
  }

  void createUser(String name, String job) async {
    //tampilkan loading
    await loading.show();
    //send data to API
    CreateUserViewModel().postDataToAPI(context, name, job).then((value) {
      setState(() {
        loading.hide();
        if (value != null) {
          print("Respon Balikan API = $value");
          String id = value.id;
          String name = value.name;
          String createdAt = value.createdAt.toString();

          ScaffoldMessenger.of(context)
              .showSnackBar(customSnackBarWithTitle(id, '$name, $createdAt'));
        }
      });
    });
  }
}
