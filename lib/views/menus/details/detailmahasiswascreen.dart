import 'dart:io';

import 'package:batch_256/models/info_mahasiswa_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class DetailMahasiswaScreen extends StatefulWidget {
  final InfoMahasiswaModel data;
  DetailMahasiswaScreen({this.data});
  @override
  DetailMahasiswaScreenState createState() => DetailMahasiswaScreenState();
}

class DetailMahasiswaScreenState extends State<DetailMahasiswaScreen> {
  File _image;
  String _nama, _tanggalLahir, _gender, _alamat, _jurusan;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nama = widget.data.nama;
    _tanggalLahir = widget.data.tanggal_lahir;
    _gender = widget.data.gender;
    _alamat = widget.data.alamat;
    _jurusan = widget.data.jurusan;

    _image = File(widget.data.foto_path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Mahasiswa'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: _image == null
                  ? Image.asset(
                      "assets/images/logo_xa.png",
                      width: 150,
                      height: 150,
                    )
                  : Image.file(
                      _image,
                      width: 150,
                      height: 150,
                    ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: [
                      Text(
                        "Nama : ",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        '$_nama',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: [
                      Text(
                        "Tanggal Lahir : ",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        '$_tanggalLahir',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: [
                      Text(
                        "Gender : ",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        '$_gender',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: [
                      Text(
                        "Alamat : ",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        '$_alamat',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: [
                      Text(
                        "Jurusan : ",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        '$_jurusan',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
