import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Parsing1 extends StatefulWidget {
  @override
  Parsing1State createState() => Parsing1State();
}

class Parsing1State extends State<StatefulWidget>
    with AutomaticKeepAliveClientMixin {
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  int responseCode = 0;
  String json100Posts = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          TextButton(
            onPressed: () {
              get100Posts();
            },
            child: Text(
              'Get 100 Posts',
            ),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.black12)),
          ),
          SizedBox(
            height: 10,
          ),
          Text("HTTP Response Code = $responseCode"),
          Expanded(
              child: ListView(
            padding: EdgeInsets.all(10),
            children: [Text('$json100Posts')],
          ))
        ],
      ),
    );
  }

  //metode untuk mengambil API
  Future<String> get100Posts() async {
    final String urlAPI = "https://jsonplaceholder.typicode.com/posts";

    var request = await http.get(Uri.parse(urlAPI), headers: {
      "X-Requested_With": "XMLHttpRequest",
      "Content-Type": "application/json"
    });

    var response = request.body;
    print('Hasil Baca API [${request.statusCode}] = $response');

    setState(() {
      responseCode = request.statusCode;
      json100Posts = request.body;
    });
  }
}
