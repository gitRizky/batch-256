import 'dart:convert';

import 'package:batch_256/components/custom_snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

class Parsing2 extends StatefulWidget {
  @override
  Parsing2State createState() => Parsing2State();
}

class Parsing2State extends State<StatefulWidget>
    with AutomaticKeepAliveClientMixin {
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  int responseCode = 0;
  List dataUsers;

  ProgressDialog loading;
  @override
  Widget build(BuildContext context) {
    //init loading (constructor)
    loading = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    loading.style(
        message: "Silahkan tunggu...",
        progressWidget: CircularProgressIndicator(),
        borderRadius: 10.0,
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          TextButton(
            onPressed: () {
              get10Users();
            },
            child: Text("Get 10 Users"),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.black12)),
          ),
          Text("HTTP Response Code = $responseCode"),
          Expanded(
            child: ListView.builder(
              itemCount: dataUsers == null ? 0 : dataUsers.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: InkWell(
                    child: Card(
                      color: index % 2 == 0 ? Colors.white : Colors.white12,
                      margin: EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Name: ${dataUsers[index]['name']}",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Email: ${dataUsers[index]['email']}",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Address: ${dataUsers[index]['address']['street']}"
                            "${dataUsers[index]['address']['suite']}"
                            "${dataUsers[index]['address']['city']}"
                            "${dataUsers[index]['address']['zipcode']}",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      String lat = dataUsers[index]["address"]['geo']['lat'];
                      String lng = dataUsers[index]["address"]['geo']['lng'];
                      String username = dataUsers[index]['username'];

                      ScaffoldMessenger.of(context).showSnackBar(
                          customSnackBarWithTitle(username, '$lat, $lng'));
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<String> get10Users() async {
    await loading.show();
    final String urlAPI = "https://jsonplaceholder.typicode.com/users";
    var request = await http.get(Uri.parse(urlAPI), headers: {});
    var response = request.body;
    print("Hasil Baca API [${request.statusCode}] = $response");

    setState(() {
      responseCode = request.statusCode;
      //konversi dari respon menjadi json agar bisa di parsing kemudian
      dataUsers = json.decode(response);
    });
    loading.hide();
  }
}
