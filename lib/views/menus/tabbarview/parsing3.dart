import 'package:batch_256/viewmodel/list_users_viewmodel.dart';
import 'package:batch_256/views/menus/details/detailuserscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:batch_256/models/list_user_model.dart';

class Parsing3 extends StatefulWidget {
  @override
  _Parsing3State createState() => _Parsing3State();
}

class _Parsing3State extends State<Parsing3>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  bool isLoading = false;
  int page = 1;
  List<Datum> dataUsers = List<Datum>.empty();
  int maxPage = 1;

  ScrollController scrollController = new ScrollController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(),
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Material(
            color: Colors.white38,
            child: TextButton(
              onPressed: () {
                page = 1;
                dataUsers = List<Datum>.empty();
                getListUsers(page);
              },
              child: Text(
                "GET List Users",
                style: TextStyle(color: Colors.black, fontSize: 16.0),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: dataUsers.length + 1,
              itemBuilder: (BuildContext context, int index) {
                if (index == dataUsers.length) {
                  return loadingIndicator();
                } else {
                  return Container(
                    child: InkWell(
                      onTap: () {
                        //pindah ke detail screen
                        //1. ambil data dari model untuk dibawa ke detail
                        Datum datum = dataUsers[index];
                        //2.pindah ke detail screen
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    DetailUserScreen(data: datum)));
                      },
                      child: Card(
                        shadowColor: CupertinoColors.white,
                        color:
                            (index % 2 == 0) ? Colors.white38 : Colors.white12,
                        margin: EdgeInsets.all(5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CircleAvatar(
                              radius: 40,
                              backgroundImage:
                                  NetworkImage(dataUsers[index].avatar),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "${dataUsers[index].firstName} ${dataUsers[index].lastName}",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "Email: ${dataUsers[index].email}",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
              },
              controller: scrollController,
            ),
          ),
        ],
      ),
    );
  }

  void getListUsers(int page) async {
//get data from API
    ListUsersViewModel().getDataFromAPI(context, page).then((value) {
      setState(() {
        isLoading = false;
        if (value != null) {
          print("Hasil Baca API = $value");
          //cari tau maxPage
          maxPage = value.totalPages;

          if (page == 1) {
            //data kosong, isi data baru
            dataUsers = value.data;
          } else {
            //sudah ada sebelumnya, tambah data baru
            dataUsers.addAll(value.data);
          }
        }
      });
    });
  }

  Widget loadingIndicator() {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 0.0,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        loadMoreData();
      }
    });
  }

  void loadMoreData() {
    if (dataUsers.isNotEmpty && page < maxPage) {
      page += 1;
      getListUsers(page);
    }
  }
}
