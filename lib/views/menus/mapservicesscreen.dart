import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class MapServicesScreen extends StatefulWidget {
  @override
  MapServicesScreenState createState() => MapServicesScreenState();
}

class MapServicesScreenState extends State<StatefulWidget> {
  static CameraPosition _initPosition = CameraPosition(
      target: LatLng(-6.236884874234397, 106.78252249670217), zoom: 12);

  static CameraPosition _xaCoordinate = CameraPosition(
      target: LatLng(-6.244800192050419, 106.79243846971433), zoom: 20);

  Completer<GoogleMapController> _controller = Completer();

  List<Marker> _markers = <Marker>[];
  static Marker _xaMarker = Marker(
      markerId: MarkerId('1'),
      position: LatLng(-6.244800192050419, 106.79243846971433),
      infoWindow: InfoWindow(title: 'Xsis Academy Campus B'),
      onTap: () async {
        String urlString = "https://xsis.academy/";
        await canLaunch(urlString)
            ? await launch(urlString)
            : throw 'Could not launch $urlString';
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Map Sevices'),
      ),
      body: GoogleMap(
        initialCameraPosition: _initPosition,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        mapType: MapType.normal,
        markers: Set<Marker>.of(_markers),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.all(10),
        child: FloatingActionButton.extended(
          onPressed: () {
            goToXsisAcademy();
          },
          label: Text('Xsis Academy'),
          icon: Icon(Icons.school),
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
    );
  }

  Future<void> goToXsisAcademy() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_xaCoordinate));
    setState(() {
      _markers.add(_xaMarker);
    });
  }
}
