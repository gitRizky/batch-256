import 'package:batch_256/components/custom_snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class LocationServicesScreen extends StatefulWidget {
  @override
  LocationServicesScreenState createState() => LocationServicesScreenState();
}

class LocationServicesScreenState extends State<StatefulWidget> {
  String _latitude, _longitute, _altitude;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location Services'),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              'Latitude = $_latitude',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Longtitude = $_longitute',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Altitude = $_altitude',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 50,
            ),
            TextButton(
              onPressed: () {
                tampilkanLokasiSaya();
              },
              child: Text(
                'Get my current location',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue)),
            )
          ],
        ),
      ),
    );
  }

  void tampilkanLokasiSaya() {
    temukanLokasiSaya().then((value) {
      String latitude = value.latitude.toString();
      String longitude = value.longitude.toString();
      String altitude = value.altitude.toString();

      setState(() {
        _latitude = latitude;
        _altitude = altitude;
        _longitute = longitude;
      });
    });
  }

  Future<Position> temukanLokasiSaya() async {
    bool serviceEnable;
    LocationPermission permission;

    //test jika location services enable
    serviceEnable = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnable) {
      ScaffoldMessenger.of(context)
          .showSnackBar(customSnackBar("Location Services disable"));
      return Future.error("Location Services disable");
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context)
            .showSnackBar(customSnackBar("Location Services disable"));
        return Future.error("Location Services disable");
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(
          customSnackBar("Location Permission Permanently Denied"));
      return Future.error("Location Permission Permanently Denied");
    }
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }
}
