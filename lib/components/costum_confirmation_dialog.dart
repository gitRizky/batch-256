import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomConfirmationDialog extends StatelessWidget {
  final String title, message, yes, no;
  final Function pressYes, pressNo;
  final Color color = Colors.white;

  const CustomConfirmationDialog(
      {this.title,
      this.message,
      this.yes,
      this.no,
      this.pressYes,
      this.pressNo});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: new Text(title),
      content: new Text(message),
      backgroundColor: color,
      titleTextStyle: TextStyle(
        color: Colors.blueAccent,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 16),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(15.0),
      ),
      actions: [
        new TextButton(
            onPressed: pressNo,
            child: new Text(
              no,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent),
            )),
        new TextButton(
            onPressed: pressYes,
            child: new Text(
              yes,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent),
            )),
      ],
    );
  }
}
