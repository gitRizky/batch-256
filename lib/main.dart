import 'package:batch_256/utilities/routes.dart';
import 'package:batch_256/views/dashboardscreen.dart';
import 'package:batch_256/views/loginscreen.dart';
import 'package:batch_256/views/loginscreenplugin.dart';
import 'package:batch_256/views/loginscreenreqres.dart';
import 'package:batch_256/views/menus/cameragalleryscreen.dart';
import 'package:batch_256/views/menus/details/detailimagescreen.dart';
import 'package:batch_256/views/menus/details/detailmahasiswascreen.dart';
import 'package:batch_256/views/menus/details/detailuserscreen.dart';
import 'package:batch_256/views/menus/imagesliderscreen.dart';
import 'package:batch_256/views/menus/inpudatamahasiswascreen.dart';
import 'package:batch_256/views/menus/listmahasiswascreen.dart';
import 'package:batch_256/views/menus/locationservicesscreen.dart';
import 'package:batch_256/views/menus/mapservicesscreen.dart';
import 'package:batch_256/views/menus/parsingdatascreen.dart';
import 'package:batch_256/views/menus/updatedatamahasiswascreen.dart';
import 'package:batch_256/views/registerscreen.dart';
import 'package:batch_256/views/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //lock screen orientation
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Batch 256',
      theme: ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.orange,
        //     cursorColor: Colors.orange),
        // theme: ThemeData(
        //   primarySwatch: Colors.lime,
      ),
      home: SplashScreen(),
      routes: {
        Routes.splashscreen: (context) => SplashScreen(),
        Routes.loginscreen: (context) => LoginScreen(),
        Routes.registerscreen: (context) => RegisterScreen(),
        Routes.dashboardscreen: (context) => DashboardScreen(),
        Routes.parsingdatascreen: (context) => ParsingDataScreen(),
        Routes.detailuserscreen: (context) => DetailUserScreen(),
        Routes.loginscreenreqres: (context) => LoginScreenReqres(),
        Routes.imagesliderscreen: (context) => ImageSliderScreen(),
        Routes.detailimagesliderscreen: (context) => DetailImageScreen(),
        Routes.loginscreenplugin: (context) => LoginScreenPlugin(),
        Routes.cameragalleryscreen: (context) => CameraGalleryScreen(),
        Routes.locationservicesscreen: (context) => LocationServicesScreen(),
        Routes.mapservicesscreen: (context) => MapServicesScreen(),
        Routes.listmahasiswascreeen: (context) => ListMahasiswaScreen(),
        Routes.inputdatamahasiswascreen: (context) =>
            InputDataMahasiswaScreen(),
        Routes.detailmahasiswascreen: (context) => DetailMahasiswaScreen(),
        Routes.updatedatamahasiswascreen: (context) =>
            UpdateDataMahasiswaScreen(),
      },
    );
  }
}
